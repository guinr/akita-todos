import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Todo } from 'src/app/models/todo.model';
import { faTrash, faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {

  faTrash = faTrash
  faSquare = faSquare
  faCheckSquare = faCheckSquare

  @Input() todo: Todo
  @Output() toggle = new EventEmitter<Todo>()
  @Output() remove = new EventEmitter<Todo>()

  toggleTodo() {
    this.toggle.emit(this.todo)
  }

  deleteTodo() {
    this.remove.emit(this.todo)
  }

}