import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { TodoState, TodoStore } from '../stores/todo.store';
import { Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodosQuery extends QueryEntity<TodoState, Todo> {

  constructor(protected store: TodoStore) {
    super(store)
  }

}