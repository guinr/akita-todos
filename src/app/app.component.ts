import { Component, OnInit } from '@angular/core';
import { TodoService } from './services/todo.service';
import { Todo } from './models/todo.model';
import { TodosQuery } from './queries/todo.query';
import { faCalendarCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  faCalendarCheck = faCalendarCheck

  todos: Todo[]

  constructor(private todosQuery: TodosQuery, private todoService: TodoService) {}

  ngOnInit() {
    this.todosQuery.selectAll().subscribe(data => {
      this.todos = data
    })
  }

  add(input: HTMLInputElement) {
    this.todoService.create(input.value)
    input.value = ''
  }

  toggle(todo: Todo) {
    this.todoService.complete(todo)
  }

  remove(todo: Todo) {
    this.todoService.delete(todo);
  }

}
