import { Injectable } from "@angular/core";
import { TodoStore } from '../stores/todo.store';
import { Todo, createTodo } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private todoStore: TodoStore) {}

  create(title: string) {
    const todo = createTodo({ title })
    this.todoStore.add(todo)
  }

  complete({ id, completed }: Todo) {
    this.todoStore.update(id, {
      completed: !completed
    })
  }

  delete({ id }: Todo) {
    this.todoStore.remove(id)
  }

}